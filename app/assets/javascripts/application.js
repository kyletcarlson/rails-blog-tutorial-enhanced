// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap
//= require_tree .


// Some helpful guides & links:
// ----------------------------

// Guide & resources: https://developer.mozilla.org/en-US/docs/Web/JavaScript

// jQuery home page: http://jquery.com
// Interactive jQuery tutorials: http://try.jquery.com

// Interactively learn Javascript for free: http://www.codecademy.com

// Create snippets of HTML/CSS/JS to show people (great for getting help): http://jsfiddle.net
// Huge list of sweet, sweet Javascript libraries: http://www.jsdb.io