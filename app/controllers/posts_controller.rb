class PostsController < ApplicationController

  # Force the user to type the correct username and password to do anything in the Posts controller except for viewing the index or a single post. Feel free to change the name/password to something else.
  http_basic_authenticate_with name: "dhh", password: "secret", except: [:index, :show]

  before_action :get_blog_post, except: [:new, :create, :index]

  def index
    # Create a @posts instance variable and fill it with all the posts in the database
    @posts = Post.all
  end

  def new
    @post = Post.new
  end

  def show
    # The before_action found the post & assigned it to @post
  end

  def edit
    # The before_action found the post & assigned it to @post
  end

  def update
    # The before_action found the post & assigned it to @post

    if @post.update(post_params)
      # Add a message in the Rails flash hash
      flash[:notice] = 'Awesomesauce! That post looks much better.'
      # If @post updated correctly, redirect the user to it
      redirect_to @post
    else
      # Send the user back to the form to try again
      render 'edit'
    end
  end

  def create
    # Create a new instance of the Post model with the params (title & body) that came from the form
    @post = Post.new(post_params)

    # Try to actually save the post to the database instead of just holding it in memory
    if @post.save
      # Add a message in the Rails flash hash
      flash[:notice] = "Success! That's one fine-looking post."
      # If @post saved correctly, redirect the user to it
      redirect_to @post
    else
      # Otherwise, send the user back to the form to try again
      render 'new'
    end
  end

  def destroy
    # The before_action found the post & assigned it to @post, so nuke it!
    @post.destroy
    # Add a message in the Rails flash hash
    flash[:notice] = 'Success! That post is history.'
    # Go back to the listing of all the posts
    redirect_to posts_path
  end


  # Methods below 'private' can only be acessed by methods inside this controller. There's no way to visit the route /posts/post_params. 
  private

    def get_blog_post
      @post = Post.find(params[:id])
    end

    # Good ol' Rails 4 Strong Parameters...
    # http://edgeguides.rubyonrails.org/action_controller_overview.html#strong-parameters
    def post_params
      # Remember how we created a new Post from the params? This line tells Rails to only allow the title and body to be set from the params. It's a major security improvement, but it seems wonky at first.
      params.require(:post).permit(:title, :text)
    end

end
