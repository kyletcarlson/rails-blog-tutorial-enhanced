class Post < ActiveRecord::Base
  # Posts have comments, and tell Rails to automagically delete all comments for a post when it is deleted
  has_many :comments, dependent: :destroy

  # Only let Posts be created that a) have a title, and b) the title has to have at least 5 characters
  validates :title, presence: true,
                    length: { minimum: 5 }
end
