module ApplicationHelper

  # Shamelessly ripped from Michael Hartl's Rails tutorial:
  # http://ruby.railstutorial.org/chapters/rails-flavored-ruby#sec-back_to_the_title_helper
  def full_title(page_title)
    base_title = "Rails Blog"

    if page_title.empty?
      base_title
    else
      "#{page_title} | #{base_title}"
    end
  end

  # Returns the meta description on a per-page basis.
  def meta_description(page_descripton)
    if page_descripton.empty?
      "A sweet blogging application inspired by the official Ruby on Rails tutorial."
    else
      "#{page_descripton}"
    end
  end

end
